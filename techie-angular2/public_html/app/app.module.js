(function (app) {
    app.AppModule =
            ng.core.NgModule({
                imports: [ng.platformBrowser.BrowserModule],
                declarations: [app.ContadorComponent],
                bootstrap: [app.ContadorComponent]
            })
            .Class({
                constructor: function () {}
            });
})(window.app || (window.app = {}));