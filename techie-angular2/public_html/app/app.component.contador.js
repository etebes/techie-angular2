(function (app) {
    app.ContadorComponent =
            ng.core.Component({
                selector: 'contador',
                templateUrl: 'app/contador.html'
            })
            .Class({
                constructor: function () {
                    this.valor = 0;
                    this.saludo="HOLA!!!";
                },
                sumar: function () {
                    this.valor++;
                },
                restar: function () {
                    this.valor--;
                }
            });
})(window.app || (window.app = {}));